Relatório de Instalação Framework Hibernate
Instalação do Hibernate
O processo de Instalação do Hibernate é simples, basta fazer o download do arquivo de instalação e descompactá-lo, logo um diretório é criado no sistema. Este diretório contém arquivo .jar núcleo do Hibernate (Hibernate3.rar) é um subdiretório lib onde encontram os arquivos .jar das outras APIs utilizadas no Hibernate. É fundamental que os arquivos .jar estejam CLASSPATH (caminho das Classes) do Projeto IDE, da IDE (Integrated Developer Environment ou da JVM (Java Virtual Machine). A classe do driver do banco de dados também estar no CLASSPATH.
Seu principal Objetivo
É diminuir a complexidade entre os programas Java, baseado no modelo orientado a objeto, que precisam trabalhar com um banco de dados do modelo relacional. Em especial, no desenvolvimento de consultas e atualizações dos dados.
Alguns riquisitos básicos para poder Utiliza-lo
NetBeans IDE JDK (Java Development Kit)
GlassFish Server Open Source Edition
Servidor de banco de dados MySQL
Link para acesso a documentação em Português
https://docs.jboss.org/hibernate/orm/3.5/reference/pt-BR/pdf/hibernate_reference.pdf
Principais passos para instalação e configuração
•	
Principais vantagens do framework
•	Escrever (bem) menos código repetitivo, sem perder flexibilidade;
•	Acesso a recursos nativos/específicos dos bancos de dados;
•	Menos preocupação da arquitetura de persistência.
Principais desvantagens encontradas no trabalho
•	Para executar consultas mais complexas, geralmente é necessário algum tempo de experiência;
•	Perda de desempenho.

